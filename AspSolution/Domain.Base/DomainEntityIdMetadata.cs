﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using com.jesori.gemestores.Contracts.Domain;


namespace Domain.Base
{
    public abstract class DomainEntityIdMetadata : DomainEntityIdMetadata<Guid>, IDomainEntityId, IDomainEntityMetadata
    {
        
    }
    public abstract class DomainEntityIdMetadata<TKey> : DomainEntityId<TKey> 
            where TKey : IEquatable<TKey>
    {
        [Display(Name = nameof(CreatedBy),
            ResourceType = typeof(Resources.Common.Common))]
        [MaxLength(256)]
        [JsonIgnore]
        public string? CreatedBy { get; set; }
        
        [Display(Name = nameof(CreatedAt),
            ResourceType = typeof(Resources.Common.Common))]
        [JsonIgnore]
        [DataType((DataType.Date))]
        public DateTime? CreatedAt { get; set; }

        [Display(Name = nameof(ChangedBy),
            ResourceType = typeof(Resources.Common.Common))]
        [MaxLength(256)]
        [JsonIgnore]
        public string? ChangedBy { get; set; }
        
        [Display(Name = nameof(ChangedAt),
            ResourceType = typeof(Resources.Common.Common))]
        [JsonIgnore]
        [DataType((DataType.Date))]

        public DateTime? ChangedAt { get; set; }
    }
}