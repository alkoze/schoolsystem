using Contracts.DAL.App.Repositories;
using com.jesori.gemestores.Contracts.DAL.Base;


namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork, IBaseEntityTracker
    {
        IAppUserRepository AppUser { get; }
        IGradeRepository Grade { get; }
        IProfileRepository Profile { get; }
        ISubjectRepository Subject { get; }
        IPossibleGradeRepository PossibleGrade { get; }
        ISemesterRepository Semester { get; }
        IUserSubjectRepository UserSubject { get; }
        
        IGradeTypeRepository GradeType { get; }
    }
}