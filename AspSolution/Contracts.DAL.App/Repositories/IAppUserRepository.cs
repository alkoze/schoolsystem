﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using com.jesori.gemestores.Contracts.DAL.Base.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Identity;

namespace Contracts.DAL.App.Repositories
{
    public interface IAppUserRepository : IBaseRepository<AppUser>
    {
        Task<IEnumerable<AppUser>> GetAllForViewAsync(Guid? id);

    }
}