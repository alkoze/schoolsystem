﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using com.jesori.gemestores.Contracts.DAL.Base.Repositories;
using DAL.App.DTO;
namespace Contracts.DAL.App.Repositories
{
    public interface IProfileRepository : IBaseRepository<Profile>
    { 
        Task<IEnumerable<Profile>> GetAllForViewAsync(Guid? id);
    }
}