﻿using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1.Identity
{
    public class DeleteAccountDTO
    {
        public string Id { get; set; } = default!;

        [MinLength(6)]
        [MaxLength(100)]
        public string Password { get; set; } = default!;
    }
}