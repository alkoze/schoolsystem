﻿using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1.Identity
{
    public class ChangePasswordDTO
    {
        
        public string Id { get; set; } = default!;

        [MinLength(6)]
        [MaxLength(100)]
        public string NewPassword { get; set; } = default!;
        
        [MinLength(6)]
        [MaxLength(100)]
        public string OldPassword { get; set; } = default!;
    }
}