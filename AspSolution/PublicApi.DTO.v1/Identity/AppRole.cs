using System;
using System.ComponentModel.DataAnnotations;
using com.jesori.gemestores.Contracts.DAL.Base;
using com.jesori.gemestores.Contracts.Domain;

namespace PublicApi.DTO.v1.Identity
{
    public class AppRole : IDomainEntityId 
    {
        public Guid Id { get; set; }
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string DisplayName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string Name { get; set; } = default!;

    }
}