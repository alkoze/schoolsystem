﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;

namespace PublicApi.DTO.v1.Identity
{
    public class EditDTO
    {
        public string Id { get; set; } = default!;
        
        [MaxLength(256)]
        [EmailAddress]
        public string Email { get; set; } = default!;

        [MinLength(1)]
        [MaxLength(128)]
        public string FirstName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(128)]
        public string LastName { get; set; } = default!;

    }
}