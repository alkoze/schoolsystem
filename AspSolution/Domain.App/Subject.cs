﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class Subject:DomainEntityIdMetadata
    {
        public string SubjectName { get; set; } = default!;
        public string SubjectCode { get; set; } = default!;
        public Guid? SubjectSemesterId { get; set; }
        public Semester? SubjectSemester { get; set; }
        public ICollection<Grade>? SubjectGrades { get; set; }
        public ICollection<UserSubject>? UserSubjects { get; set; }
    }
}