﻿using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class PossibleGrade:DomainEntityIdMetadata
    {
        public string GradeName { get; set; } = default!;
        
        public ICollection<Grade>? Grades { get; set; }

    }
}