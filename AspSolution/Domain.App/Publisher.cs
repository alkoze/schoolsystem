﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class Publisher : DomainEntityIdMetadata
    {
        public string PublisherName { get; set; } = default!;
        public string Country { get; set; } = default!;
        
        // public ICollection<Game>? Games { get; set; }
    }
}