﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class Role:DomainEntityIdMetadata
    {
        public string RoleName { get; set; } = default!;
        
        public ICollection<Profile>? Profiles { get; set; }

    }
}