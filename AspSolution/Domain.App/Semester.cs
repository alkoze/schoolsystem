﻿using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class Semester:DomainEntityIdMetadata
    {
        public string SemesterName { get; set; } = default!;
        
        public ICollection<Subject>? Subjects { get; set; }
    }
}