﻿using System;
using System.Collections.Generic;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class UserSubject:DomainEntityIdMetadata
    {
        public Guid? ProfileId { get; set; }
        public Profile? Profile { get; set; }
        public Guid? SubjectId { get; set; }
        public Subject? Subject { get; set; }
        public Boolean? Accepted { get; set; }
    }
}