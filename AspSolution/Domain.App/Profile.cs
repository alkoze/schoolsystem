﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Profile: DomainEntityIdMetadata
    {
        public Guid AppUserId { get; set; } = default!;
        public AppUser? AppUser { get; set; }
        [MaxLength(128)] [MinLength(1)] public string LastName { get; set; } = default!;
        public Guid? RoleId { get; set; }
        [MaxLength(128)] [MinLength(1)] public Role? AppRole { get; set; }

        public ICollection<Grade>? Grades { get; set; }

        public ICollection<UserSubject>? UserSubjects { get; set; }
    }
}