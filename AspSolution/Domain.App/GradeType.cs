﻿using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class GradeType:DomainEntityIdMetadata
    {
        public string GradeTypeName { get; set; } = default!;
        
        public ICollection<Grade>? Grades { get; set; }

    }
}