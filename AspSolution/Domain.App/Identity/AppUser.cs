using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using com.jesori.gemestores.Contracts.Domain;
using Microsoft.AspNetCore.Identity;

namespace Domain.App.Identity
{
    public class AppUser : IdentityUser<Guid>,IDomainEntityId
    {

       
        [MaxLength(128)] [MinLength(1)] public string FirstName { get; set; } = default!;
        [MaxLength(128)] [MinLength(1)] public string LastName { get; set; } = default!;

        public Guid? ProfileId { get; set; }
        public Profile? Profile { get; set; }

        // [MaxLength(128)] [MinLength(1)] public string NickName { get; set; } = default!;

        
        
        // public ICollection<StoreAccount>? StoreAccounts { get; set; }
        // public ICollection<GameReview>? GameReviews { get; set; }
        // public ICollection<StoreReview>? StoreReviews { get; set; }
        //
        // public ICollection<Comment>? Comments { get; set; }
        //
        // public ICollection<Rating>? Ratings { get; set; }
        //
        // public ICollection<User_payment_method>? UserPaymentMethods { get; set; }
        //
        // public ICollection<Bill>? Bills { get; set; }
        //
    }
}