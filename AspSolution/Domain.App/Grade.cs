﻿using System;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Grade:DomainEntityIdMetadataUser<AppUser>
    {
        public string Description { get; set; } = default!;
        public Guid? PossibleGradeId { get; set; } = default!;
        public PossibleGrade? PossibleGrade { get; set; }
        public Guid? GradeSubjectId { get; set; }
        public Subject? GradeSubject { get; set; }
        public Guid? ProfileId { get; set; }
        public Profile? Profile { get; set; }

        public Guid? GradeTypeId { get; set; }

        public GradeType? GradeType { get; set; }
    }
}