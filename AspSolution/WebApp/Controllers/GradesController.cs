using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    public class GradesController : Controller
    {
        private readonly AppDbContext _context;

        public GradesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Grades
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Grades.Include(g => g.AppUser).Include(g => g.GradeSubject).Include(g => g.GradeType).Include(g => g.PossibleGrade).Include(g => g.Profile);
            return View(await appDbContext.ToListAsync());
        }

        // GET: Grades/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grade = await _context.Grades
                .Include(g => g.AppUser)
                .Include(g => g.GradeSubject)
                .Include(g => g.GradeType)
                .Include(g => g.PossibleGrade)
                .Include(g => g.Profile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (grade == null)
            {
                return NotFound();
            }

            return View(grade);
        }

        // GET: Grades/Create
        public IActionResult Create()
        {
            ViewData["AppUserId"] = new SelectList(_context.AppUsers, "Id", "FirstName");
            ViewData["GradeSubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode");
            ViewData["GradeTypeId"] = new SelectList(_context.GradeTypes, "Id", "GradeTypeName");
            ViewData["PossibleGradeId"] = new SelectList(_context.PossibleGrades, "Id", "GradeName");
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName");
            return View();
        }

        // POST: Grades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Description,PossibleGradeId,GradeSubjectId,ProfileId,GradeTypeId,AppUserId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] Grade grade)
        {
            if (ModelState.IsValid)
            {
                grade.Id = Guid.NewGuid();
                _context.Add(grade);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.AppUsers, "Id", "FirstName", grade.AppUserId);
            ViewData["GradeSubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode", grade.GradeSubjectId);
            ViewData["GradeTypeId"] = new SelectList(_context.GradeTypes, "Id", "GradeTypeName", grade.GradeTypeId);
            ViewData["PossibleGradeId"] = new SelectList(_context.PossibleGrades, "Id", "GradeName", grade.PossibleGradeId);
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName", grade.ProfileId);
            return View(grade);
        }

        // GET: Grades/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grade = await _context.Grades.FindAsync(id);
            if (grade == null)
            {
                return NotFound();
            }
            ViewData["AppUserId"] = new SelectList(_context.AppUsers, "Id", "FirstName", grade.AppUserId);
            ViewData["GradeSubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode", grade.GradeSubjectId);
            ViewData["GradeTypeId"] = new SelectList(_context.GradeTypes, "Id", "GradeTypeName", grade.GradeTypeId);
            ViewData["PossibleGradeId"] = new SelectList(_context.PossibleGrades, "Id", "GradeName", grade.PossibleGradeId);
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName", grade.ProfileId);
            return View(grade);
        }

        // POST: Grades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Description,PossibleGradeId,GradeSubjectId,ProfileId,GradeTypeId,AppUserId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] Grade grade)
        {
            if (id != grade.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(grade);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GradeExists(grade.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.AppUsers, "Id", "FirstName", grade.AppUserId);
            ViewData["GradeSubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode", grade.GradeSubjectId);
            ViewData["GradeTypeId"] = new SelectList(_context.GradeTypes, "Id", "GradeTypeName", grade.GradeTypeId);
            ViewData["PossibleGradeId"] = new SelectList(_context.PossibleGrades, "Id", "GradeName", grade.PossibleGradeId);
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName", grade.ProfileId);
            return View(grade);
        }

        // GET: Grades/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grade = await _context.Grades
                .Include(g => g.AppUser)
                .Include(g => g.GradeSubject)
                .Include(g => g.GradeType)
                .Include(g => g.PossibleGrade)
                .Include(g => g.Profile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (grade == null)
            {
                return NotFound();
            }

            return View(grade);
        }

        // POST: Grades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var grade = await _context.Grades.FindAsync(id);
            _context.Grades.Remove(grade);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GradeExists(Guid id)
        {
            return _context.Grades.Any(e => e.Id == id);
        }
    }
}
