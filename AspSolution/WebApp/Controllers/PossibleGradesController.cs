using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    public class PossibleGradesController : Controller
    {
        private readonly AppDbContext _context;

        public PossibleGradesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: PossibleGrades
        public async Task<IActionResult> Index()
        {
            return View(await _context.PossibleGrades.ToListAsync());
        }

        // GET: PossibleGrades/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var possibleGrade = await _context.PossibleGrades
                .FirstOrDefaultAsync(m => m.Id == id);
            if (possibleGrade == null)
            {
                return NotFound();
            }

            return View(possibleGrade);
        }

        // GET: PossibleGrades/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PossibleGrades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GradeName,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] PossibleGrade possibleGrade)
        {
            if (ModelState.IsValid)
            {
                possibleGrade.Id = Guid.NewGuid();
                _context.Add(possibleGrade);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(possibleGrade);
        }

        // GET: PossibleGrades/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var possibleGrade = await _context.PossibleGrades.FindAsync(id);
            if (possibleGrade == null)
            {
                return NotFound();
            }
            return View(possibleGrade);
        }

        // POST: PossibleGrades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("GradeName,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] PossibleGrade possibleGrade)
        {
            if (id != possibleGrade.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(possibleGrade);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PossibleGradeExists(possibleGrade.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(possibleGrade);
        }

        // GET: PossibleGrades/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var possibleGrade = await _context.PossibleGrades
                .FirstOrDefaultAsync(m => m.Id == id);
            if (possibleGrade == null)
            {
                return NotFound();
            }

            return View(possibleGrade);
        }

        // POST: PossibleGrades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var possibleGrade = await _context.PossibleGrades.FindAsync(id);
            _context.PossibleGrades.Remove(possibleGrade);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PossibleGradeExists(Guid id)
        {
            return _context.PossibleGrades.Any(e => e.Id == id);
        }
    }
}
