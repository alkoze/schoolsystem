using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    public class UserSubjectsController : Controller
    {
        private readonly AppDbContext _context;

        public UserSubjectsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: UserSubjects
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.UserSubjects.Include(u => u.Profile).Include(u => u.Subject);
            return View(await appDbContext.ToListAsync());
        }

        // GET: UserSubjects/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userSubject = await _context.UserSubjects
                .Include(u => u.Profile)
                .Include(u => u.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (userSubject == null)
            {
                return NotFound();
            }

            return View(userSubject);
        }

        // GET: UserSubjects/Create
        public IActionResult Create()
        {
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName");
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode");
            return View();
        }

        // POST: UserSubjects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProfileId,SubjectId,Accepted,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] UserSubject userSubject)
        {
            if (ModelState.IsValid)
            {
                userSubject.Id = Guid.NewGuid();
                _context.Add(userSubject);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName", userSubject.ProfileId);
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode", userSubject.SubjectId);
            return View(userSubject);
        }

        // GET: UserSubjects/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userSubject = await _context.UserSubjects.FindAsync(id);
            if (userSubject == null)
            {
                return NotFound();
            }
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName", userSubject.ProfileId);
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode", userSubject.SubjectId);
            return View(userSubject);
        }

        // POST: UserSubjects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ProfileId,SubjectId,Accepted,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] UserSubject userSubject)
        {
            if (id != userSubject.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userSubject);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserSubjectExists(userSubject.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProfileId"] = new SelectList(_context.Profiles, "Id", "LastName", userSubject.ProfileId);
            ViewData["SubjectId"] = new SelectList(_context.Subjects, "Id", "SubjectCode", userSubject.SubjectId);
            return View(userSubject);
        }

        // GET: UserSubjects/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userSubject = await _context.UserSubjects
                .Include(u => u.Profile)
                .Include(u => u.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (userSubject == null)
            {
                return NotFound();
            }

            return View(userSubject);
        }

        // POST: UserSubjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var userSubject = await _context.UserSubjects.FindAsync(id);
            _context.UserSubjects.Remove(userSubject);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserSubjectExists(Guid id)
        {
            return _context.UserSubjects.Any(e => e.Id == id);
        }
    }
}
