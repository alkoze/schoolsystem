using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GPS location types - Location, WayPoint, CheckPoint
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class SemestersController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppUnitOfWork _uow;

        /// <summary>
        /// Constructor
        /// </summary>
        public SemestersController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        /// <summary>
        /// Get the predefined Semester collection.
        /// Regular location update, WayPoint, CheckPoint
        /// </summary>
        /// <returns>List of available Semesters</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Semester>))]
        public async Task<ActionResult<IEnumerable<Semester>>> GetSemesters()
        {
            return Ok(await _uow.Semester.GetAllAsync());
        }

        /// <summary>
        /// Get single Semester
        /// </summary>
        /// <param name="id">Semester Id</param>
        /// <returns>request Semester</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Semester))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Semester>> GetSemester(Guid id)
        {
            var Semester = await _uow.Semester.FirstOrDefaultAsync(id);

            if (Semester == null)
            {
                return NotFound(new MessageDTO("Semester not found"));
            }

            return Ok(Semester);
        }

        /// <summary>
        /// Update the Semester
        /// </summary>
        /// <param name="id">Semester id</param>
        /// <param name="Semester">Semester object</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageDTO))]
        public async Task<IActionResult> PutSemester(Guid id, DAL.App.DTO.Semester Semester)
        {
            if (id != Semester.Id)
            {
                return BadRequest(new MessageDTO("id and Semester.id do not match"));
            }

            await _uow.Semester.UpdateAsync(Semester);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Semester
        /// </summary>
        /// <param name="Semester">Semester object</param>
        /// <returns>created Semester object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Semester))]
        public async Task<ActionResult<Semester>> PostSemester(DAL.App.DTO.Semester Semester)
        {
            _uow.Semester.Add(Semester);
            await _uow.SaveChangesAsync();
            
            // FIXME - how to get back the object just now added to database.....
            // we need this: Semester = _uow.GetUpdatedEntity(Semester);
            return CreatedAtAction("GetSemester",
                new {id = Semester.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                Semester);
        }

        /// <summary>
        /// Delete the Semester
        /// </summary>
        /// <param name="id">Semester Id</param>
        /// <returns>deleted Semester object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Semester))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Semester>> DeleteSemester(Guid id)
        {
            var Semester = await _uow.Semester.FirstOrDefaultAsync(id);
            if (Semester == null)
            {
                return NotFound(new MessageDTO("Semester not found"));
            }

            await _uow.Semester.RemoveAsync(Semester);
            await _uow.SaveChangesAsync();

            return Ok(Semester);
        }
    }
}
