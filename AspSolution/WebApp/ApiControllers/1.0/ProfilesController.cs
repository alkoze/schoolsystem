using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GPS location types - Location, WayPoint, CheckPoint
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ProfilesController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppUnitOfWork _uow;

        /// <summary>
        /// Constructor
        /// </summary>
        public ProfilesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        /// <summary>
        /// Get the predefined Profile collection.
        /// Regular location update, WayPoint, CheckPoint
        /// </summary>
        /// <returns>List of available Profiles</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Profile>))]
        public async Task<ActionResult<IEnumerable<Profile>>> GetProfiles()
        {
            return Ok(await _uow.Profile.GetAllForViewAsync(null));
        }

        /// <summary>
        /// Get single Profile
        /// </summary>
        /// <param name="id">Profile Id</param>
        /// <returns>request Profile</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Profile))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Profile>> GetProfile(Guid id)
        {
            var Profile = await _uow.Profile.GetAllForViewAsync(id);

            if (Profile == null)
            {
                return NotFound(new MessageDTO("Profile not found"));
            }

            return Ok(Profile.FirstOrDefault());
        }

        /// <summary>
        /// Update the Profile
        /// </summary>
        /// <param name="id">Profile id</param>
        /// <param name="Profile">Profile object</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageDTO))]
        public async Task<IActionResult> PutProfile(Guid id, DAL.App.DTO.Profile Profile)
        {
            if (id != Profile.Id)
            {
                return BadRequest(new MessageDTO("id and Profile.id do not match"));
            }

            await _uow.Profile.UpdateAsync(Profile);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Profile
        /// </summary>
        /// <param name="Profile">Profile object</param>
        /// <returns>created Profile object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Profile))]
        public async Task<ActionResult<Profile>> PostProfile(DAL.App.DTO.Profile Profile)
        {
            _uow.Profile.Add(Profile);
            await _uow.SaveChangesAsync();
            
            // FIXME - how to get back the object just now added to database.....
            // we need this: Profile = _uow.GetUpdatedEntity(Profile);
            return CreatedAtAction("GetProfile",
                new {id = Profile.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                Profile);
        }

        /// <summary>
        /// Delete the Profile
        /// </summary>
        /// <param name="id">Profile Id</param>
        /// <returns>deleted Profile object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Profile))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Profile>> DeleteProfile(Guid id)
        {
            var Profile = await _uow.Profile.FirstOrDefaultAsync(id);
            if (Profile == null)
            {
                return NotFound(new MessageDTO("Profile not found"));
            }

            await _uow.Profile.RemoveAsync(Profile);
            await _uow.SaveChangesAsync();

            return Ok(Profile);
        }
    }
}
