using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GPS location types - Location, WayPoint, CheckPoint
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class GradesController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppUnitOfWork _uow;

        /// <summary>
        /// Constructor
        /// </summary>
        public GradesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        /// <summary>
        /// Get the predefined Grade collection.
        /// Regular location update, WayPoint, CheckPoint
        /// </summary>
        /// <returns>List of available Grades</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Grade>))]
        public async Task<ActionResult<IEnumerable<Grade>>> GetGrades()
        {
            return Ok(await _uow.Grade.GetAllForViewAsync(null));
        }

        /// <summary>
        /// Get single Grade
        /// </summary>
        /// <param name="id">Grade Id</param>
        /// <returns>request Grade</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Grade))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Grade>> GetGrade(Guid id)
        {
            var grade = await _uow.Grade.GetAllForViewAsync(id);

            if (grade == null)
            {
                return NotFound(new MessageDTO("Grade not found"));
            }

            return Ok(grade.First());
        }

        /// <summary>
        /// Update the Grade
        /// </summary>
        /// <param name="id">Grade id</param>
        /// <param name="Grade">Grade object</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageDTO))]
        public async Task<IActionResult> PutGrade(Guid id, DAL.App.DTO.Grade Grade)
        {
            if (id != Grade.Id)
            {
                return BadRequest(new MessageDTO("id and Grade.id do not match"));
            }

            await _uow.Grade.UpdateAsync(Grade);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Grade
        /// </summary>
        /// <param name="Grade">Grade object</param>
        /// <returns>created Grade object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Grade))]
        public async Task<ActionResult<Grade>> PostGrade(DAL.App.DTO.Grade Grade)
        {
            _uow.Grade.Add(Grade);
            await _uow.SaveChangesAsync();
            
            // FIXME - how to get back the object just now added to database.....
            // we need this: Grade = _uow.GetUpdatedEntity(Grade);
            return CreatedAtAction("GetGrade",
                new {id = Grade.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                Grade);
        }

        /// <summary>
        /// Delete the Grade
        /// </summary>
        /// <param name="id">Grade Id</param>
        /// <returns>deleted Grade object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Grade))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Grade>> DeleteGrade(Guid id)
        {
            var Grade = await _uow.Grade.FirstOrDefaultAsync(id);
            if (Grade == null)
            {
                return NotFound(new MessageDTO("Grade not found"));
            }

            await _uow.Grade.RemoveAsync(Grade);
            await _uow.SaveChangesAsync();

            return Ok(Grade);
        }
    }
}
