using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GPS location types - Location, WayPoint, CheckPoint
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class SubjectsController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppUnitOfWork _uow;

        /// <summary>
        /// Constructor
        /// </summary>
        public SubjectsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        /// <summary>
        /// Get the predefined Subject collection.
        /// Regular location update, WayPoint, CheckPoint
        /// </summary>
        /// <returns>List of available Subjects</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Subject>))]
        public async Task<ActionResult<IEnumerable<Subject>>> GetSubjects()
        {
            return Ok(await _uow.Subject.GetAllForViewAsync(null));
        }

        /// <summary>
        /// Get single Subject
        /// </summary>
        /// <param name="id">Subject Id</param>
        /// <returns>request Subject</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Subject))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Subject>> GetSubject(Guid id)
        {
            var Subject = await _uow.Subject.GetAllForViewAsync(id);

            if (Subject == null)
            {
                return NotFound(new MessageDTO("Subject not found"));
            }

            return Ok(Subject.First());
        }

        /// <summary>
        /// Update the Subject
        /// </summary>
        /// <param name="id">Subject id</param>
        /// <param name="Subject">Subject object</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageDTO))]
        public async Task<IActionResult> PutSubject(Guid id, DAL.App.DTO.Subject Subject)
        {
            if (id != Subject.Id)
            {
                return BadRequest(new MessageDTO("id and Subject.id do not match"));
            }

            await _uow.Subject.UpdateAsync(Subject);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Subject
        /// </summary>
        /// <param name="Subject">Subject object</param>
        /// <returns>created Subject object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Subject))]
        public async Task<ActionResult<Subject>> PostSubject(DAL.App.DTO.Subject Subject)
        {
            _uow.Subject.Add(Subject);
            await _uow.SaveChangesAsync();
            
            // FIXME - how to get back the object just now added to database.....
            // we need this: Subject = _uow.GetUpdatedEntity(Subject);
            return CreatedAtAction("GetSubject",
                new {id = Subject.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                Subject);
        }

        /// <summary>
        /// Delete the Subject
        /// </summary>
        /// <param name="id">Subject Id</param>
        /// <returns>deleted Subject object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Subject))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<Subject>> DeleteSubject(Guid id)
        {
            var Subject = await _uow.Subject.FirstOrDefaultAsync(id);
            if (Subject == null)
            {
                return NotFound(new MessageDTO("Subject not found"));
            }

            await _uow.Subject.RemoveAsync(Subject);
            await _uow.SaveChangesAsync();

            return Ok(Subject);
        }
    }
}
