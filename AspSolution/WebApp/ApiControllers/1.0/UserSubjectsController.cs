using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GPS location types - Location, WayPoint, CheckPoint
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class UserSubjectsController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppUnitOfWork _uow;

        /// <summary>
        /// Constructor
        /// </summary>
        public UserSubjectsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        /// <summary>
        /// Get the predefined UserSubject collection.
        /// Regular location update, WayPoint, CheckPoint
        /// </summary>
        /// <returns>List of available UserSubjects</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<UserSubject>))]
        public async Task<ActionResult<IEnumerable<UserSubject>>> GetUserSubjects()
        {
            return Ok(await _uow.UserSubject.GetAllForViewAsync(null));
        }

        /// <summary>
        /// Get single UserSubject
        /// </summary>
        /// <param name="id">UserSubject Id</param>
        /// <returns>request UserSubject</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserSubject))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<UserSubject>> GetUserSubject(Guid id)
        {
            var UserSubject = await _uow.UserSubject.GetAllForViewAsync(id);

            if (UserSubject == null)
            {
                return NotFound(new MessageDTO("UserSubject not found"));
            }

            return Ok(UserSubject.FirstOrDefault());
        }

        /// <summary>
        /// Update the UserSubject
        /// </summary>
        /// <param name="id">UserSubject id</param>
        /// <param name="UserSubject">UserSubject object</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageDTO))]
        public async Task<IActionResult> PutUserSubject(Guid id, DAL.App.DTO.UserSubject UserSubject)
        {
            if (id != UserSubject.Id)
            {
                return BadRequest(new MessageDTO("id and UserSubject.id do not match"));
            }

            await _uow.UserSubject.UpdateAsync(UserSubject);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new UserSubject
        /// </summary>
        /// <param name="userSubject">UserSubject object</param>
        /// <returns>created UserSubject object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(UserSubject))]
        public async Task<ActionResult<UserSubject>> PostUserSubject(DAL.App.DTO.UserSubject userSubject)
        {
            _uow.UserSubject.Add(userSubject);
            await _uow.SaveChangesAsync();
            
            // FIXME - how to get back the object just now added to database.....
            // we need this: UserSubject = _uow.GetUpdatedEntity(UserSubject);
            return CreatedAtAction("GetUserSubject",
                new {id = userSubject.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                userSubject);
        }

        /// <summary>
        /// Delete the UserSubject
        /// </summary>
        /// <param name="id">UserSubject Id</param>
        /// <returns>deleted UserSubject object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserSubject))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<UserSubject>> DeleteUserSubject(Guid id)
        {
            var UserSubject = await _uow.UserSubject.FirstOrDefaultAsync(id);
            if (UserSubject == null)
            {
                return NotFound(new MessageDTO("UserSubject not found"));
            }

            await _uow.UserSubject.RemoveAsync(UserSubject);
            await _uow.SaveChangesAsync();

            return Ok(UserSubject);
        }
    }
}
