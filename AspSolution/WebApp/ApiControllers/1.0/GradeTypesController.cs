using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GPS location types - Location, WayPoint, CheckPoint
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class GradeTypesController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppUnitOfWork _uow;

        /// <summary>
        /// Constructor
        /// </summary>
        public GradeTypesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        /// <summary>
        /// Get the predefined GradeType collection.
        /// Regular location update, WayPoint, CheckPoint
        /// </summary>
        /// <returns>List of available GradeTypes</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<GradeType>))]
        public async Task<ActionResult<IEnumerable<GradeType>>> GetGradeTypes()
        {
            return Ok(await _uow.GradeType.GetAllAsync());
        }

        /// <summary>
        /// Get single GradeType
        /// </summary>
        /// <param name="id">GradeType Id</param>
        /// <returns>request GradeType</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GradeType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<GradeType>> GetGradeType(Guid id)
        {
            var GradeType = await _uow.GradeType.FirstOrDefaultAsync(id);

            if (GradeType == null)
            {
                return NotFound(new MessageDTO("GradeType not found"));
            }

            return Ok(GradeType);
        }

        /// <summary>
        /// Update the GradeType
        /// </summary>
        /// <param name="id">GradeType id</param>
        /// <param name="GradeType">GradeType object</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageDTO))]
        public async Task<IActionResult> PutGradeType(Guid id, DAL.App.DTO.GradeType GradeType)
        {
            if (id != GradeType.Id)
            {
                return BadRequest(new MessageDTO("id and GradeType.id do not match"));
            }

            await _uow.GradeType.UpdateAsync(GradeType);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new GradeType
        /// </summary>
        /// <param name="GradeType">GradeType object</param>
        /// <returns>created GradeType object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GradeType))]
        public async Task<ActionResult<GradeType>> PostGradeType(DAL.App.DTO.GradeType GradeType)
        {
            _uow.GradeType.Add(GradeType);
            await _uow.SaveChangesAsync();
            
            // FIXME - how to get back the object just now added to database.....
            // we need this: GradeType = _uow.GetUpdatedEntity(GradeType);
            return CreatedAtAction("GetGradeType",
                new {id = GradeType.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                GradeType);
        }

        /// <summary>
        /// Delete the GradeType
        /// </summary>
        /// <param name="id">GradeType Id</param>
        /// <returns>deleted GradeType object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GradeType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<GradeType>> DeleteGradeType(Guid id)
        {
            var GradeType = await _uow.GradeType.FirstOrDefaultAsync(id);
            if (GradeType == null)
            {
                return NotFound(new MessageDTO("GradeType not found"));
            }

            await _uow.GradeType.RemoveAsync(GradeType);
            await _uow.SaveChangesAsync();

            return Ok(GradeType);
        }
    }
}
