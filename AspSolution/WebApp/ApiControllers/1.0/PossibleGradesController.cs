using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GPS location types - Location, WayPoint, CheckPoint
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class PossibleGradesController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppUnitOfWork _uow;

        /// <summary>
        /// Constructor
        /// </summary>
        public PossibleGradesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        /// <summary>
        /// Get the predefined PossibleGrade collection.
        /// Regular location update, WayPoint, CheckPoint
        /// </summary>
        /// <returns>List of available PossibleGrades</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<PossibleGrade>))]
        public async Task<ActionResult<IEnumerable<PossibleGrade>>> GetPossibleGrades()
        {
            return Ok(await _uow.PossibleGrade.GetAllAsync());
        }

        /// <summary>
        /// Get single PossibleGrade
        /// </summary>
        /// <param name="id">PossibleGrade Id</param>
        /// <returns>request PossibleGrade</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PossibleGrade))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<PossibleGrade>> GetPossibleGrade(Guid id)
        {
            var PossibleGrade = await _uow.PossibleGrade.FirstOrDefaultAsync(id);

            if (PossibleGrade == null)
            {
                return NotFound(new MessageDTO("PossibleGrade not found"));
            }

            return Ok(PossibleGrade);
        }

        /// <summary>
        /// Update the PossibleGrade
        /// </summary>
        /// <param name="id">PossibleGrade id</param>
        /// <param name="PossibleGrade">PossibleGrade object</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageDTO))]
        public async Task<IActionResult> PutPossibleGrade(Guid id, DAL.App.DTO.PossibleGrade PossibleGrade)
        {
            if (id != PossibleGrade.Id)
            {
                return BadRequest(new MessageDTO("id and PossibleGrade.id do not match"));
            }

            await _uow.PossibleGrade.UpdateAsync(PossibleGrade);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new PossibleGrade
        /// </summary>
        /// <param name="PossibleGrade">PossibleGrade object</param>
        /// <returns>created PossibleGrade object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(PossibleGrade))]
        public async Task<ActionResult<PossibleGrade>> PostPossibleGrade(DAL.App.DTO.PossibleGrade PossibleGrade)
        {
            _uow.PossibleGrade.Add(PossibleGrade);
            await _uow.SaveChangesAsync();
            
            // FIXME - how to get back the object just now added to database.....
            // we need this: PossibleGrade = _uow.GetUpdatedEntity(PossibleGrade);
            return CreatedAtAction("GetPossibleGrade",
                new {id = PossibleGrade.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                PossibleGrade);
        }

        /// <summary>
        /// Delete the PossibleGrade
        /// </summary>
        /// <param name="id">PossibleGrade Id</param>
        /// <returns>deleted PossibleGrade object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PossibleGrade))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<PossibleGrade>> DeletePossibleGrade(Guid id)
        {
            var PossibleGrade = await _uow.PossibleGrade.FirstOrDefaultAsync(id);
            if (PossibleGrade == null)
            {
                return NotFound(new MessageDTO("PossibleGrade not found"));
            }

            await _uow.PossibleGrade.RemoveAsync(PossibleGrade);
            await _uow.SaveChangesAsync();

            return Ok(PossibleGrade);
        }
    }
}
