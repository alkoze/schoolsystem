﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class PossibleGrade:DomainEntityId
    {
        // public Guid Id { get; set; }

        public string GradeName { get; set; } = default!;
    }
}