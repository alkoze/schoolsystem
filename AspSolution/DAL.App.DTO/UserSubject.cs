﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class UserSubject:DomainEntityId
    {
        // public Guid Id { get; set; }
        public Guid? ProfileId { get; set; }
        public Domain.App.Profile? Profile { get; set; }
        public Guid? SubjectId { get; set; }
        public Domain.App.Subject? Subject { get; set; }
        public Boolean? Accepted { get; set; }
    }
}