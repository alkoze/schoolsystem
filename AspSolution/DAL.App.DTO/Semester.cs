﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Semester:DomainEntityId
    {
        // public Guid Id { get; set; }
        public string SemesterName { get; set; } = default!;
    }
}