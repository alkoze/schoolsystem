﻿using System;
using System.Collections.Generic;
using Domain.App;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Subject:DomainEntityId
    {
        // public Guid Id { get; set; }
        public string SubjectName { get; set; } = default!;
        public string SubjectCode { get; set; } = default!;
        public Guid? SubjectSemesterId { get; set; }
        public string? SubjectSemester { get; set; }
        public ICollection<Grade> SubjectGrades { get; set; }
        public ICollection<Profile>? Profiles { get; set; }
    }
}