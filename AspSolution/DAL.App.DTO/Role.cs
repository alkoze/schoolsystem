﻿using System;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Role:DomainEntityId
    {
        // public Guid Id { get; set; }
        public string RoleName { get; set; } = default!;
    }
}