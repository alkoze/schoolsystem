﻿using System;
using System.Linq;
using Domain.App;
using Domain.App.Identity;
using Domain.Base;
using AppUser = DAL.App.DTO.Identity.AppUser;

namespace DAL.App.DTO
{
    public class Grade:DomainEntityId
    {
        // public Guid Id { get; set; }

        public string Description { get; set; } = default!;
        public Guid? PossibleGradeId { get; set; } = default!;
        public string? PossibleGrade { get; set; }
        public Guid AppUserId { get; set; }
        public string? AppUser { get; set; }

        public Guid? GradeSubjectId { get; set; }
        public string? GradeSubject { get; set; }
        public Guid? ProfileId { get; set; }
        public string? Profile { get; set; }

        public Guid? GradeTypeId { get; set; }

        public string? GradeType { get; set; }
    }
}