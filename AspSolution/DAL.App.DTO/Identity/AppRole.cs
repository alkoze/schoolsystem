using System;
using System.ComponentModel.DataAnnotations;
using com.jesori.gemestores.Contracts.Domain;
using Microsoft.AspNetCore.Identity;

namespace DAL.App.DTO.Identity
{
    public class AppRole : IdentityRole<Guid>, IDomainEntityId
    {
        [MinLength(1)]
        [MaxLength(256)]
        public string DisplayName { get; set; } = default!;
    }
}