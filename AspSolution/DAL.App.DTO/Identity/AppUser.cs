using System;
using System.ComponentModel.DataAnnotations;
using com.jesori.gemestores.Contracts.Domain;
using Microsoft.AspNetCore.Identity;

namespace DAL.App.DTO.Identity
{
    public class AppUser : IDomainEntityId
    {


        public Guid Id { get; set; }
        [MinLength(1)]
        [MaxLength(128)]
        [Required]
        public string FirstName { get; set; } = default!;
        [MinLength(1)]
        [MaxLength(128)]
        [Required]
        public string LastName { get; set; } = default!;
        public Guid? ProfileId { get; set; }
        public string? Profile { get; set; }
        // [MaxLength(128)] [MinLength(1)] public string NickName { get; set; } = default!;

        

        // public ICollection<StoreAccount>? StoreAccounts { get; set; }
        // public ICollection<Review>? Reviews { get; set; }
        // //
        // public ICollection<Comment>? Comments { get; set; }
        //
        // public ICollection<Rating>? Ratings { get; set; }
        //
        // public ICollection<User_payment_method>? UserPaymentMethods { get; set; }
        //
        // public ICollection<Bill>? Bills { get; set; }
        //
    }
}