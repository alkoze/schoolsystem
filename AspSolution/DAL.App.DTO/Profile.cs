﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.App;
using Domain.App.Identity;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Profile: DomainEntityId
    {
        // public Guid Id { get; set; }
        public Guid AppUserId { get; set; } = default!;
        [MaxLength(128)] [MinLength(1)] public string LastName { get; set; } = default!;
        
        public Guid? RoleId { get; set; }
        [MaxLength(128)] [MinLength(1)] public string? Role { get; set; }

        public ICollection<DAL.App.DTO.Grade>? Grades { get; set; }

        public ICollection<Subject>? Subjects { get; set; }
    }
}