﻿using Domain.Base;

namespace DAL.App.DTO
{
    public class GradeType: DomainEntityId
    {
        public string GradeTypeName { get; set; } = default!;
    }
}