﻿using System;
using System.Collections.Generic;
using com.jesori.gemestores.Contracts.Domain;

namespace DAL.App.DTO
{
    public class Publisher : IDomainEntityId
    {
        public Guid Id { get; set; }

        public string PublisherName { get; set; } = default!;
        public string Country { get; set; } = default!;
        
        // public ICollection<Game>? Games { get; set; }
    }
}