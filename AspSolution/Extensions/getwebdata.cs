﻿using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Extensions
{
    public class getwebdata
    {
        public string GetActialPrice(string source)
        {
            var text = "";
            var htmlWeb = new HtmlWeb();

            if (source.Contains("steam"))
            {
                Regex rx = new Regex(@"(\/app\/)(\d+)");
                MatchCollection matches = rx.Matches(source);
                var json = new WebClient().DownloadString("https://store.steampowered.com/api/appdetails/?appids=" + matches.First().ToString().Substring(5, matches.First().ToString().Length-5));
                rx = new Regex(@"(?<=final_formatted"":"")(\d+,\d+)");
                matches = rx.Matches(json);
                text = matches.First().ToString();
            }

            if (source.Contains("gog"))
            {
                var documentNode = htmlWeb.Load(source).DocumentNode;

                try
                {
                    // text = documentNode.Descendants("span").Where(d => d.Attributes["class"].Value.Contains("product-actions-price__final-amount")).First()
                    //     .InnerHtml.Trim();
                    var findclasses =
                        documentNode.SelectNodes("//span[contains(@class,'product-actions-price__final-amount')]");
                    text = findclasses.Select(x => x.InnerText).First();
                    if (text.Contains(".")) text = text.Replace(".", ",");
                }
                catch (Exception)
                {
                    return text;
                }
            }

            if (source.Contains("ubi"))
            {
                var documentNode = htmlWeb.Load(source).DocumentNode;

                try
                {
                    var findclasses =
                        documentNode.SelectNodes(
                            "//div[@class='flex-reverse-order']//span[contains(@class, 'price-sales standard-price')]");
                    text = findclasses.Select(x => x.InnerText).First().Replace("&#8364;", "").Trim();
                }
                catch (Exception)
                {
                    return text;
                }
            }

            return text;
        }
    }
}