﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using com.jesori.gemestores.Contracts.DAL.Base;
using com.jesori.gemestores.Contracts.Domain;
using Domain;
using Domain.App;
using Domain.App.Identity;
using Domain.Base;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AppUser = Domain.App.Identity.AppUser;
using Publisher = Domain.App.Publisher;

namespace DAL.App.EF
{
    public class AppDbContext: IdentityDbContext<AppUser, AppRole, Guid>, IBaseEntityTracker
    {
        private IUserNameProvider _userNameProvider;
        
        public DbSet<AppUser> AppUsers { get; set; } = default!;

        public DbSet<Role> PersonRoles { get; set; } = default!;

        public DbSet<Grade> Grades { get; set; } = default!;
        
        public DbSet<GradeType> GradeTypes { get; set; } = default!;
        public DbSet<PossibleGrade> PossibleGrades { get; set; } = default!;

        public DbSet<Profile> Profiles { get; set; } = default!;

        public DbSet<Semester> Semesters { get; set; } = default!;

        public DbSet<Subject> Subjects { get; set; } = default!;

        public DbSet<UserSubject> UserSubjects { get; set; } = default!;

        public DbSet<Publisher> Publishers { get; set; } = default!;
        public DbSet<LangStr> LangStrs { get; set; } = default!;
        public DbSet<LangStrTranslation> LangStrTranslation { get; set; } = default!;
        
        
        private readonly Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>> _entityTracker =
            new Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>>();

        public AppDbContext(DbContextOptions<AppDbContext> options, IUserNameProvider userNameProvider) : base(options)
        {
            _userNameProvider = userNameProvider;
        }
        
        public void AddToEntityTracker(IDomainEntityId<Guid> internalEntity, IDomainEntityId<Guid> externalEntity)
        {
            _entityTracker.Add(internalEntity, externalEntity);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // disable cascade delete
            foreach (var relationship in builder.Model
                .GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }


            // enable cascade delete on Category->GameCategory
            builder.Entity<Profile>()
                .HasMany(s => s.Grades)
                .WithOne(l => l.Profile)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Profile>()
                .HasMany(s => s.UserSubjects)
                .WithOne(l => l.Profile)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Subject>()
                .HasMany(s => s.UserSubjects)
                .WithOne(l => l.Subject)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Subject>()
                .HasMany(s => s.SubjectGrades)
                .WithOne(l => l.GradeSubject)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<GradeType>()
                .HasMany(s => s.Grades)
                .WithOne(l => l.GradeType)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<AppUser>()
                .HasOne(a => a.Profile)
                .WithOne(a => a.AppUser)
                .HasForeignKey<Profile>(c => c.AppUserId);
            
            builder.Entity<Profile>()
                .HasOne(a => a.AppUser)
                .WithOne(a => a.Profile)
                .HasForeignKey<AppUser>(c => c.ProfileId);
            
            builder.Entity<LangStrTranslation>().HasIndex(i => new {i.Culture, i.LangStrId}).IsUnique();
        }

        private void SaveChangesMetadataUpdate()
        {
            // update the state of ef tracked objects
            ChangeTracker.DetectChanges();

            var markedAsAdded = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);
            foreach (var entityEntry in markedAsAdded)
            {
                if (entityEntry.Entity is DomainEntityIdMetadataUser<AppUser> entityWithMetaDataUser)
                {

                    entityWithMetaDataUser.CreatedAt = DateTime.Now;
                    entityWithMetaDataUser.CreatedBy = _userNameProvider.CurrentUserName;
                    entityWithMetaDataUser.ChangedAt = entityWithMetaDataUser.CreatedAt;
                    entityWithMetaDataUser.ChangedBy = entityWithMetaDataUser.CreatedBy;
                }
                else if (entityEntry.Entity is DomainEntityIdMetadata entityWithMetaData)
                {
                    entityWithMetaData.CreatedAt = DateTime.Now;
                    entityWithMetaData.CreatedBy = _userNameProvider.CurrentUserName;
                    entityWithMetaData.ChangedAt = entityWithMetaData.CreatedAt;
                    entityWithMetaData.ChangedBy = entityWithMetaData.CreatedBy;
                }
            }

            var markedAsModified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);
            foreach (var entityEntry in markedAsModified)
            {
                if (entityEntry.Entity is DomainEntityIdMetadataUser<AppUser> entityWithMetaDataUser)
                {

                    entityWithMetaDataUser.ChangedAt = DateTime.Now;
                    entityWithMetaDataUser.ChangedBy = _userNameProvider.CurrentUserName;

                    entityEntry.Property(nameof(entityWithMetaDataUser.CreatedAt)).IsModified = false;
                    entityEntry.Property(nameof(entityWithMetaDataUser.CreatedBy)).IsModified = false;
                }
                else if (entityEntry.Entity is DomainEntityIdMetadata entityWithMetaData)
                {
                    entityWithMetaData.ChangedAt = DateTime.Now;
                    entityWithMetaData.ChangedBy = entityWithMetaData.CreatedBy;

                    entityEntry.Property(nameof(entityWithMetaData.CreatedAt)).IsModified = false;
                    entityEntry.Property(nameof(entityWithMetaData.CreatedBy)).IsModified = false;
                }
            }
        }

        private void UpdateTrackedEntities()
        {
            foreach (var (key, value) in _entityTracker)
            {
                value.Id = key.Id;
            }
        }

        public override int SaveChanges()
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChanges();
            UpdateTrackedEntities();
            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChangesAsync(cancellationToken);
            UpdateTrackedEntities();
            return result;
        }
    }
}