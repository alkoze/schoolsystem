﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using com.jesori.gemestores.DAL.Base.EF.Repositories;
using com.jesori.gemestores.DAL.Base.Mappers;
using Domain;
using Domain.App;
using Domain.App.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class SubjectRepository  : EFBaseRepository<AppDbContext,AppUser,Subject,DTO.Subject>,ISubjectRepository
    {
        public SubjectRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<Subject, DTO.Subject>())
        {
        }

        public virtual async Task<IEnumerable<DTO.Subject>> GetAllForViewAsync(Guid? id)
        {
            List<DTO.Subject> result;
            IQueryable<Subject> query = RepoDbSet;
            if (id != null)
            {
                query = query.Where(item => item.Id == id);
            }
            result = await query
                .Select(a=> new  DTO.Subject()
                {
                    Id = a.Id,
                    SubjectName = a.SubjectName,
                    SubjectCode = a.SubjectCode,
                    SubjectSemesterId = a.SubjectSemesterId,
                    SubjectSemester = a.SubjectSemester.SemesterName,
                    SubjectGrades = a.SubjectGrades.Select(b => new DAL.App.DTO.Grade()
                    {
                        Id = b.Id,
                        Description = b.Description,
                        AppUserId = b.AppUserId,
                        AppUser = b.AppUser.FirstName + " " + b.AppUser.FirstName,
                        PossibleGradeId = b.PossibleGradeId,
                        PossibleGrade = b.PossibleGrade.GradeName,
                        Profile = RepoDbContext.AppUsers.First(r => r.Id == b.Profile.AppUserId).FirstName + " " + RepoDbContext.AppUsers.First(r => r.Id == b.Profile.AppUserId).LastName,
                        ProfileId = b.ProfileId,
                        GradeTypeId = b.GradeTypeId,
                        GradeType = b.GradeType.GradeTypeName
                    }).ToList(),
                    Profiles = a.UserSubjects.Select(c => c.Profile).Select(d => new DAL.App.DTO.Profile()
                    {
                        Id = d.Id,
                        RoleId = d.RoleId,
                        Role = RepoDbContext.PersonRoles.First(z => z.Id == d.RoleId).RoleName,
                        AppUserId = d.AppUserId,
                        LastName = RepoDbContext.AppUsers.First(j => j.Id == d.AppUserId).FirstName + " " + RepoDbContext.AppUsers.First(j => j.Id == d.AppUserId).LastName,
                    }).ToList()
                }).ToListAsync();
            return result;
        }
    }
}