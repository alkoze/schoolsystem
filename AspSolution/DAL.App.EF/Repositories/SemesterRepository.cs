﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using com.jesori.gemestores.DAL.Base.EF.Repositories;
using com.jesori.gemestores.DAL.Base.Mappers;
using Domain;
using Domain.App;
using Domain.App.Identity;

namespace DAL.App.EF.Repositories
{
    public class SemesterRepository  : EFBaseRepository<AppDbContext,AppUser,Semester,DTO.Semester>,ISemesterRepository
    {
        public SemesterRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<Semester, DTO.Semester>())
        {
        }
    }
}