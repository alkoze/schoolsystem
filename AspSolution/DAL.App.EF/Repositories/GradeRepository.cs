﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using com.jesori.gemestores.DAL.Base.EF.Repositories;
using com.jesori.gemestores.DAL.Base.Mappers;
using Domain;
using Domain.App;
using Domain.App.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class GradeRepository  : EFBaseRepository<AppDbContext,AppUser,Grade,DTO.Grade>,IGradeRepository
    {
        public GradeRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<Grade, DTO.Grade>())
        {
        }
        
        // public override async Task<IEnumerable<DTO.Grade>> GetAllAsync(object? userId = null, bool noTracking = true)
        // {
        //     var query = PrepareQuery(userId, noTracking);
        //     query = query
        //         .Include(g => g.PossibleGrade)
        //         .Include(g => g.GradeSubject)
        //         .Include(g => g.Profile)
        //         .Include(g => g.AppUser);
        //     // .ThenInclude(g => g.Store);
        //     var domainItems = await query.ToListAsync();
        //     var result = domainItems.Select(e => Mapper.Map(e));
        //     return result;
        // }
        public virtual async Task<IEnumerable<DTO.Grade>> GetAllForViewAsync(Guid? id)
        { 
            // .Include(a => a.GameInStores)
                         // .ThenInclude(a => a.Store)
            List<DTO.Grade> result;
            IQueryable<Grade> query = RepoDbSet;
            if (id != null)
            {
                query = query.Where(item => item.Id == id);
            }
            result = await query
                .Select(a=> new  DTO.Grade()
                {
                    Id = a.Id,
                    Description = a.Description,
                    AppUserId = a.AppUserId,
                    AppUser = a.AppUser.FirstName + " " + a.AppUser.FirstName,
                    PossibleGradeId = a.PossibleGradeId,
                    PossibleGrade = a.PossibleGrade.GradeName,
                    ProfileId = a.ProfileId,
                    Profile = RepoDbContext.AppUsers.First(r => r.Id == a.Profile.AppUserId).FirstName + " " + RepoDbContext.AppUsers.First(r => r.Id == a.Profile.AppUserId).LastName,
                    GradeSubjectId = a.GradeSubjectId,
                    GradeSubject = a.GradeSubject.SubjectName+ " " +a.GradeSubject.SubjectCode,
                    GradeTypeId = a.GradeTypeId,
                    GradeType = a.GradeType.GradeTypeName
                }).ToListAsync();
            return result;
        }

    }
}