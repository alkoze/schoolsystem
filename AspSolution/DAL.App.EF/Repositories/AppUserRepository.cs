﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using com.jesori.gemestores.DAL.Base.EF.Repositories;
using com.jesori.gemestores.DAL.Base.Mappers;
using Domain;
using Domain.App;
using Domain.App.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class AppUserRepository  : EFBaseRepository<AppDbContext,AppUser,AppUser,DTO.Identity.AppUser>,IAppUserRepository
    {
        public AppUserRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<AppUser, DTO.Identity.AppUser>())
        {
        }
        public virtual async Task<IEnumerable<DTO.Identity.AppUser>> GetAllForViewAsync(Guid? id)
        { 
            // .Include(a => a.GameInStores)
            // .ThenInclude(a => a.Store)
            List<DTO.Identity.AppUser> result;
            IQueryable<AppUser> query = RepoDbSet;
            if (id != null)
            {
                query = query.Where(item => item.Id == id);
            }
            result = await query
                .Select(a=> new  DTO.Identity.AppUser()
                {
                    Id = a.Id,
                    FirstName = a.FirstName,
                    LastName= a.LastName,
                    ProfileId = RepoDbContext.Profiles.First(i => i.AppUserId == id).Id,
                    Profile = RepoDbContext.Profiles.First(i => i.AppUserId == id).AppRole.RoleName,
                    
                    
                    
                }).ToListAsync();
            return result;
        }
    }
}