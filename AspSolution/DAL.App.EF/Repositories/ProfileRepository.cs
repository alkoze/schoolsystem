﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using com.jesori.gemestores.DAL.Base.EF.Repositories;
using com.jesori.gemestores.DAL.Base.Mappers;
using Domain;
using Domain.App;
using Domain.App.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ProfileRepository  : EFBaseRepository<AppDbContext,AppUser,Profile,DTO.Profile>,IProfileRepository
    {
        public ProfileRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<Profile, DTO.Profile>())
        {
        }

        public virtual async Task<IEnumerable<DTO.Profile>> GetAllForViewAsync(Guid? id)
        {
            List<DTO.Profile> result;
            IQueryable<Profile> query = RepoDbSet;
            if (id != null)
            {
                query = query.Where(item => item.Id == id);
            }
            result = await query
                .Select(a=> new  DTO.Profile()
                {
                    Id = a.Id,
                    RoleId = a.RoleId,
                    Role = a.AppRole.RoleName,
                    AppUserId = a.AppUserId,
                    LastName = RepoDbContext.AppUsers.First(r => r.Id == a.AppUserId).FirstName + " " + RepoDbContext.AppUsers.First(r => r.Id == a.AppUserId).LastName,
                    Grades = a.Grades.Select(b => new DAL.App.DTO.Grade()
                    {
                        Id = b.Id,
                        Description = b.Description,
                        GradeSubjectId = b.GradeSubjectId,
                        GradeSubject = b.GradeSubject.SubjectName+ " " +b.GradeSubject.SubjectCode,
                        PossibleGradeId = b.PossibleGradeId,
                        PossibleGrade = b.PossibleGrade.GradeName,
                        Profile = RepoDbContext.AppUsers.First(r => r.Id == b.Profile.AppUserId).FirstName + " " + RepoDbContext.AppUsers.First(r => r.Id == b.Profile.AppUserId).LastName,
                        ProfileId = b.ProfileId,
                        GradeTypeId = b.GradeTypeId,
                        GradeType = b.GradeType.GradeTypeName
                    }).ToList(),
                    Subjects = a.UserSubjects.Select(c => c.Subject).Select(d => new DAL.App.DTO.Subject()
                    {
                        Id = d.Id,
                        SubjectName = d.SubjectName,
                        SubjectCode = d.SubjectCode,
                        SubjectSemesterId = d.SubjectSemesterId,
                        SubjectSemester = d.SubjectSemester.SemesterName
                    }).ToList()
                }).ToListAsync();
            return result;
        }
    }
}