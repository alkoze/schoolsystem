﻿using com.jesori.gemestores.DAL.Base.EF.Repositories;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using Domain.App;
using Domain.App.Identity;

namespace DAL.App.EF.Repositories
{
    public class GradeTypeRepository: EFBaseRepository<AppDbContext,AppUser,GradeType,DTO.GradeType>,IGradeTypeRepository
    {
        public GradeTypeRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<GradeType, DTO.GradeType>())
        {
        }
    }
}