﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using com.jesori.gemestores.DAL.Base.EF.Repositories;
using com.jesori.gemestores.DAL.Base.Mappers;
using Domain;
using Domain.App;
using Domain.App.Identity;

namespace DAL.App.EF.Repositories
{
    public class PossibleGradeRepository  : EFBaseRepository<AppDbContext,AppUser,PossibleGrade,DTO.PossibleGrade>,IPossibleGradeRepository
    {
        public PossibleGradeRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<PossibleGrade, DTO.PossibleGrade>())
        {
        }
    }
}