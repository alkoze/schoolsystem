﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using com.jesori.gemestores.DAL.Base.EF.Repositories;
using com.jesori.gemestores.DAL.Base.Mappers;
using Domain;
using Domain.App;
using Domain.App.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class UserSubjectRepository  : EFBaseRepository<AppDbContext,AppUser,UserSubject,DTO.UserSubject>,IUserSubjectRepository
    {
        public UserSubjectRepository(AppDbContext dbContext) : base(dbContext, new DALMapper<UserSubject, DTO.UserSubject>())
        {
        }
        public virtual async Task<IEnumerable<DTO.UserSubject>> GetAllForViewAsync(Guid? id)
        {
            List<DTO.UserSubject> result;
            IQueryable<UserSubject> query = RepoDbSet;
            if (id != null)
            {
                query = query.Where(item => item.Id == id);
            }
            result = await query
                .Select(a=> new  DTO.UserSubject()
                {
                    Id = a.Id,
                    SubjectId = a.SubjectId,
                    ProfileId = a.ProfileId,
                    Subject = RepoDbContext.Subjects.First(s => s.Id == a.SubjectId),
                    Profile = RepoDbContext.Profiles.First(s => s.Id == a.ProfileId),
                    Accepted = a.Accepted
                    
                }).ToListAsync();
            return result;
        }
    }
}