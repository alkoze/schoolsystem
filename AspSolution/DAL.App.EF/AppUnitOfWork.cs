using System;
using System.Collections.Generic;
using System.ComponentModel;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using com.jesori.gemestores.Contracts.DAL.Base;
using DAL.App.EF.Repositories;
using com.jesori.gemestores.DAL.Base.EF;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class AppUnitOfWork : EFBaseUnitOfWork<Guid, AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfWork(AppDbContext uowDbContext) : base(uowDbContext)
        {
        }

        public IAppUserRepository AppUser =>
            GetRepository<IAppUserRepository>(() => new AppUserRepository(UowDbContext));
        public IGradeRepository Grade =>
            GetRepository<IGradeRepository>(() => new GradeRepository(UowDbContext));
        public IGradeTypeRepository GradeType =>
            GetRepository<IGradeTypeRepository>(() => new GradeTypeRepository(UowDbContext));
        public IProfileRepository Profile =>
            GetRepository<IProfileRepository>(() => new ProfileRepository(UowDbContext));
        public IPublisherRepository Publisher =>
            GetRepository<IPublisherRepository>(() => new PublisherRepository(UowDbContext));
        public ISubjectRepository Subject =>
            GetRepository<ISubjectRepository>(() => new SubjectRepository(UowDbContext));
        public IPossibleGradeRepository PossibleGrade =>
            GetRepository<IPossibleGradeRepository>(() => new PossibleGradeRepository(UowDbContext));
        public ISemesterRepository Semester =>
            GetRepository<ISemesterRepository>(() => new SemesterRepository(UowDbContext));
        public IUserSubjectRepository UserSubject =>
            GetRepository<IUserSubjectRepository>(() => new UserSubjectRepository(UowDbContext));
        // public IBaseRepository<Book> Books =>
            // GetRepository<IBaseRepository<Book>>(() => new EFBaseRepository<Book, AppDbContext>(UOWDbContext));
    }
}