using AutoMapper;
using com.jesori.gemestores.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class DALMapper<TLeftObject, TRightObject> : BaseMapper<TLeftObject, TRightObject>
        where TRightObject : class?, new()
        where TLeftObject : class?, new()
    {
        public DALMapper() : base()
        { 
            // add more mappings
            
            MapperConfigurationExpression.CreateMap<Domain.App.Identity.AppUser, DAL.App.DTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<Domain.App.Grade, DAL.App.DTO.Grade>();
            MapperConfigurationExpression.CreateMap<Domain.App.PossibleGrade, DAL.App.DTO.PossibleGrade>();
            MapperConfigurationExpression.CreateMap<Domain.App.Profile, DAL.App.DTO.Profile>();
            MapperConfigurationExpression.CreateMap<Domain.App.Role, DAL.App.DTO.Role>();
            MapperConfigurationExpression.CreateMap<Domain.App.Semester, DAL.App.DTO.Semester>();
            MapperConfigurationExpression.CreateMap<Domain.App.Subject, DAL.App.DTO.Subject>();
            MapperConfigurationExpression.CreateMap<Domain.App.UserSubject, DAL.App.DTO.UserSubject>();
            MapperConfigurationExpression.CreateMap<Domain.App.GradeType, DAL.App.DTO.GradeType>();

            
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppUser, Domain.App.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Grade, Domain.App.Grade>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.PossibleGrade, Domain.App.PossibleGrade>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Profile, Domain.App.Profile>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Role, Domain.App.Role>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Semester, Domain.App.Semester>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Subject, Domain.App.Subject>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.UserSubject, Domain.App.UserSubject>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.GradeType, Domain.App.GradeType>();


            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}