import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import AccountLogin from '../views/Account/Login.vue'
import AccountRegister from '../views/Account/Register.vue'
import SubjectIndex from '../views/Subject/Index.vue'
import SubjectDetails from '../views/Subject/Details.vue'
import MarkCreate from '../views/Mark/Create.vue'
import SubjectMark from '../views/Mark/SubjectMark.vue'
import MySubjects from '../views/Subject/MySubjects.vue'
import MyMark from '../views/Mark/MyMark.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    { path: '/', name: 'Home', component: Home },
    { path: '/account/login', name: 'AccountLogin', component: AccountLogin },
    { path: '/account/register', name: 'AccountRegister', component: AccountRegister },
    { path: '/subject', name: 'Subject', component: SubjectIndex },
    { path: '/subject/details/:id?', name: 'SubjectDetails', component: SubjectDetails, props: true },
    { path: '/mark/create/:id?', name: 'MarkCreate', component: MarkCreate, props: true },
    { path: '/mark/subjectmark/:id?', name: 'SubjectMark', component: SubjectMark, props: true },
    { path: '/mysubjects', name: 'MySubjects', component: MySubjects },
    { path: '/mymark/:id?', name: 'MyMark', component: MyMark, props: true }
]

const router = new VueRouter({
    routes
})

export default router
