import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import { ILoginDTO } from '@/types/ILoginDTO';
import { AccountApi } from '@/services/AccountApi';
import { IRegisterDTO } from '@/types/IRegisterDTO';
import { ISubjects } from '@/domain/ISubjects';
import { IProfile } from '@/domain/IProfile';
import { SubjectApi } from '@/services/SubjectApi';
import { ProfileApi } from '@/services/ProfileApi';
import { IUserSubjectDTO } from '@/types/IUserSubjectDTO';
import { UserSubjectApi } from '@/services/UserSubjectApi';
import { IUserSubject } from '@/domain/IUserSubject';
import { IGradeType } from '@/domain/IGradeType';
import { GradeTypeApi } from '@/services/GradeTypeApi';
import { IPossibleGrades } from '@/domain/IPossibleGrades';
import { PossibleGradeApi } from '@/services/PossibleGradeApi';
import { IGrade } from '@/domain/IGrade';
import { GradeApi } from '@/services/GradeApi';
import { IGradeDTO } from '@/types/IGradeDTO';
import { ISemesters } from '@/domain/ISemesters';
import { SemesterApi } from '@/services/SemestersApi';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        jwt: null as string | null,
        register: {} as IRegisterDTO,
        subjects: {} as ISubjects[],
        subject: {} as ISubjects,
        profiles: {} as IProfile[],
        profile: {} as IProfile,
        userSubject: {} as IUserSubjectDTO,
        userSubjects: {} as IUserSubject[],
        oneUserSubject: {} as IUserSubject,
        gradeTypes: {} as IGradeType[],
        possibleGrades: {} as IPossibleGrades[],
        grades: {} as IGrade[],
        grade: {} as IGrade,
        semesters: {} as ISemesters[]
    },
    mutations: {
        setJwt(state, jwt: string | null) {
            state.jwt = jwt;
        },
        setSemesters(state, semesters: ISemesters[]) {
            state.semesters = semesters;
        },
        setGradeTypes(state, subjects: IGradeType[]) {
            state.gradeTypes = subjects
        },
        setPossibleGrades(state, subjects: IPossibleGrades[]) {
            state.possibleGrades = subjects
        },
        setGrades(state, subjects: IGrade[]) {
            state.grades = subjects
        },
        setGrade(state, grade: IGrade) {
            state.grade = grade
        },
        setRegister(state, register: IRegisterDTO) {
            state.register = register
        },
        setSubjects(state, subjects: ISubjects[]) {
            state.subjects = subjects
        },
        setProfiles(state, profiles: IProfile[]) {
            state.profiles = profiles
        },
        setProfile(state, profile: IProfile) {
            state.profile = profile
        },
        setNewUserProfile(state, newUserSubject: IUserSubjectDTO) {
            state.userSubject = newUserSubject
        },
        setUserSubjects(state, userSubjects: IUserSubject[]) {
            state.userSubjects = userSubjects
        },
        setOneUserSubjects(state, oneUserSubject: IUserSubject) {
            state.oneUserSubject = oneUserSubject
        },
        setSubject(state, subject: ISubjects) {
            state.subject = subject;
        }
    },
    getters: {
        isAuthenticated(context): boolean {
            return context.jwt !== null;
        }
    },
    actions: {
        clearJwt(context): void {
            context.commit('setJwt', null);
        },
        async authenticateUser(context, loginDTO: ILoginDTO): Promise<boolean> {
            const jwt = await AccountApi.getJwt(loginDTO);
            await context.commit('setJwt', jwt);
            await context.dispatch("getProfiles");
            await context.dispatch("getSubjects");
            await context.dispatch('getUserSubjects');
            return jwt !== null;
        },
        async register(context, registerDTO: IRegisterDTO): Promise<boolean> {
            const acc = await AccountApi.register(registerDTO);
            await context.dispatch("getProfiles");
            await context.dispatch("getSubjects");
            await context.dispatch('getUserSubjects');
            return acc;
        },
        async getGradeTypes(context): Promise<void> {
            const subjects = await GradeTypeApi.getAll();
            await context.commit('setGradeTypes', subjects)
        },
        async getPossibleGrades(context): Promise<void> {
            const subjects = await PossibleGradeApi.getAll();
            await context.commit('setPossibleGrades', subjects)
        },
        async getGrades(context): Promise<void> {
            const subjects = await GradeApi.getAll();
            await context.commit('setGrades', subjects)
        },
        async getSemesters(context): Promise<void> {
            const subjects = await SemesterApi.getAll();
            await context.commit('setSemesters', subjects)
        },
        async getOneGrade(context, id: string): Promise<void> {
            const subject = await GradeApi.getById(id);
            await context.commit('setSubject', subject)
        },
        async createUserGrade(context, gradeDTO: IGradeDTO): Promise<boolean> {
            const newGrade = await GradeApi.create(gradeDTO);
            await context.dispatch('getGrades')
            await context.dispatch('getSubjects')
            return newGrade
        },
        async getSubjects(context): Promise<void> {
            const subjects = await SubjectApi.getAll();
            await context.commit('setSubjects', subjects)
        },
        async getOneSubject(context, id: string): Promise<void> {
            const subject = await SubjectApi.getById(id);
            await context.commit('setSubject', subject)
        },
        async getProfiles(context): Promise<void> {
            const profiles = await ProfileApi.getAll();
            await context.commit('setProfiles', profiles)
        },
        async getOneProfile(context, id: string): Promise<void> {
            const subject = await ProfileApi.getById(id);
            await context.commit('setProfile', subject)
        },
        async createUserSubject(context, userSubjectDTO: IUserSubjectDTO): Promise<boolean> {
            const newUserSubject = await UserSubjectApi.create(userSubjectDTO);
            await context.dispatch('getUserSubjects')
            await context.dispatch('getSubjects')
            return newUserSubject
        },
        async getUserSubjects(context): Promise<void> {
            const userSubjects = await UserSubjectApi.getAll();
            await context.commit('setUserSubjects', userSubjects)
        },
        async updateUserSubject(context, userSubject: IUserSubject): Promise<boolean> {
            const updated = await UserSubjectApi.update(userSubject);
            await UserSubjectApi.update(userSubject)
            await context.dispatch('getSubjects')
            await context.dispatch('getUserSubjects')
            return updated
        },
        async getOneUserSubject(context, id: string): Promise<void> {
            const subject = await UserSubjectApi.getById(id);
            await context.commit('setOneUserSubjects', subject)
        },
        async deleteUserSubject(context, id: string): Promise<void> {
            console.log('deleteItem', context.getters.isAuthenticated);
            await UserSubjectApi.delete(id);
            await context.dispatch("getProfiles");
            await context.dispatch("getSubjects");
            await context.dispatch('getUserSubjects');
        }
    },
    modules: {
    }
})
