export interface IGrade {
  description: string,
    possibleGradeId: string,
    possibleGrade: string,
    appUserId: string,
    appUser: string,
    gradeSubjectId: string,
    gradeSubject: string,
    profileId: string,
    profile: string,
    gradeTypeId: string,
        gradeType: string,
    id: string
}