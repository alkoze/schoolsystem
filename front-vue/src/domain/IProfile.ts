import { ISubjects } from "./ISubjects";
import { IGrade } from "./IGrade";

export interface IProfile {
  appUserId: string,
    lastName: string,
    roleId: string,
    role: string,
    subjects: ISubjects[]
    grades: IGrade[],
    id: string
}