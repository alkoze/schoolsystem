import { IProfile } from "./IProfile";
import { IGrade } from "./IGrade";

export interface ISubjects {
  subjectName: string,
    subjectCode: string,
    subjectSemesterId: string,
    subjectSemester: string,
    profiles: IProfile[],
    subjectGrades: IGrade[],
    id: string
}