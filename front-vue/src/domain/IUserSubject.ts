import { IProfile } from './IProfile';
import { ISubjects } from './ISubjects';

export interface IUserSubject{
  profileId: string,
    subjectId: string,
    accepted: boolean,
    id: string,
    profile: IProfile,
    subject: ISubjects
}