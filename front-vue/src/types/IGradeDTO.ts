export interface IGradeDTO{
  description: string,
  possibleGradeId: string,
  appUserId: string,
  gradeSubjectId: string,
  profileId: string,
  gradeTypeId: string,
  id: string
}