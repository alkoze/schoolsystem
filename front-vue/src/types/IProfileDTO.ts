export interface IProfileDTO{
  appUserId: string,
  lastName: string,
  roleId: string,
  role: string,
  id: string
}