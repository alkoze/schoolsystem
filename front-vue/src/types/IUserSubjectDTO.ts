export interface IUserSubjectDTO{
  profileId: string,
  subjectId: string,
  accepted: boolean,
  id: string
}