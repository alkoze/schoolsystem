import Axios from 'axios';
import { IUserSubject } from '../domain/IUserSubject';
import { IUserSubjectDTO } from '@/types/IUserSubjectDTO';

export abstract class UserSubjectApi {
    private static axios = Axios.create(
        {
            baseURL: "https://localhost:5001/api/v1/UserSubjects/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IUserSubject[]> {
        const url = "";
        try {
            const response = await this.axios.get<IUserSubject[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async create(itemInCartDTO: IUserSubjectDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<IUserSubjectDTO>(url, itemInCartDTO);
            console.log('ItemInCart response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async update(item: IUserSubject): Promise<boolean> {
        const url = "" + item.id
        try {
            const response = await this.axios.put<IUserSubject>(url, item);
            console.log('item response', response.status);
            if (response.status === 204) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async getById(id: string): Promise<IUserSubject> {
        const url = "" + id;
        try {
            const response = await this.axios.get<IUserSubject>(url);
            console.log('getById response', response);
            if (response.status === 200) {
                return response.data;
            }
            return response.data;
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return error;
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "" + id;
        try {
            const response = await this.axios.delete<IUserSubject>(url);
            console.log('delete response', response);
            if (response.status === 200) {
                return;
            }
            return;
        } catch (error) {
            console.log('error: ', (error as Error).message);
        }
    }
}
