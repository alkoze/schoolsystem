import Axios from 'axios';
import { IPossibleGrades } from '../domain/IPossibleGrades';

export abstract class PossibleGradeApi {
  private static axios = Axios.create(
      {
          baseURL: "https://localhost:5001/api/v1/PossibleGrades/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IPossibleGrades[]> {
      const url = "";
      try {
          const response = await this.axios.get<IPossibleGrades[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}