import Axios from 'axios'
import { IGradeType } from '@/domain/IGradeType';

export abstract class GradeTypeApi {
    private static axios = Axios.create(
        {
            baseURL: "https://localhost:5001/api/v1/GradeTypes/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IGradeType[]> {
        const url = "";
        try {
            const response = await this.axios.get<IGradeType[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async getById(id: string): Promise<IGradeType> {
        const url = "" + id;
        try {
            const response = await this.axios.get<IGradeType>(url);
            console.log('getById response', response);
            if (response.status === 200) {
                return response.data;
            }
            return response.data;
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return error;
        }
    }
}
