import Axios from 'axios'
import { IProfile } from '../domain/IProfile';
import { IProfileDTO } from '@/types/IProfileDTO';

export abstract class ProfileApi {
    private static axios = Axios.create(
        {
            baseURL: "https://localhost:5001/api/v1/Profiles/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IProfile[]> {
        const url = "";
        try {
            const response = await this.axios.get<IProfile[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async getById(id: string): Promise<IProfile> {
        const url = "" + id;
        try {
            const response = await this.axios.get<IProfile>(url);
            console.log('getById response', response);
            if (response.status === 200) {
                return response.data;
            }
            return response.data;
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return error;
        }
    }

    static async create(itemDTO: IProfileDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<IProfileDTO>(url, itemDTO);
            console.log('item response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async update(item: IProfile): Promise<boolean> {
        const url = "" + item.id
        try {
            const response = await this.axios.put<IProfile>(url, item);
            console.log('item response', response.status);
            if (response.status === 204) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async delete(id: string, jwt: string): Promise<void> {
        const url = "" + id;
        try {
            const response = await this.axios.delete<IProfile>(url, { headers: { Authorization: 'Bearer ' + jwt } });
            console.log('delete response', response);
            if (response.status === 200) {
                return;
            }
            return;
        } catch (error) {
            console.log('error: ', (error as Error).message);
        }
    }
}
