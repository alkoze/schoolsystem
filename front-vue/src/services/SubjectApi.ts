import Axios from 'axios';
import { ISubjects } from '../domain/ISubjects';

export abstract class SubjectApi {
    private static axios = Axios.create(
        {
            baseURL: "https://localhost:5001/api/v1/Subjects/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<ISubjects[]> {
        const url = "";
        try {
            const response = await this.axios.get<ISubjects[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async getById(id: string): Promise<ISubjects> {
        const url = "" + id;
        try {
            const response = await this.axios.get<ISubjects>(url);
            console.log('getById response', response);
            if (response.status === 200) {
                return response.data;
            }
            return response.data;
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return error;
        }
    }
}
