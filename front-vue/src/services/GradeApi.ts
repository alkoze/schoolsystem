import Axios from 'axios'
import { IGrade } from '../domain/IGrade';
import { IGradeDTO } from '@/types/IGradeDTO';

export abstract class GradeApi {
    private static axios = Axios.create(
        {
            baseURL: "https://localhost:5001/api/v1/Grades/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IGrade[]> {
        const url = "";
        try {
            const response = await this.axios.get<IGrade[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async getById(id: string): Promise<IGrade> {
        const url = "" + id;
        try {
            const response = await this.axios.get<IGrade>(url);
            console.log('getById response', response);
            if (response.status === 200) {
                return response.data;
            }
            return response.data;
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return error;
        }
    }

    static async create(itemDTO: IGradeDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<IGradeDTO>(url, itemDTO);
            console.log('item response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async update(item: IGrade): Promise<boolean> {
        const url = "" + item.id
        try {
            const response = await this.axios.put<IGrade>(url, item);
            console.log('item response', response.status);
            if (response.status === 204) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async delete(id: string, jwt: string): Promise<void> {
        const url = "" + id;
        try {
            const response = await this.axios.delete<IGrade>(url, { headers: { Authorization: 'Bearer ' + jwt } });
            console.log('delete response', response);
            if (response.status === 200) {
                return;
            }
            return;
        } catch (error) {
            console.log('error: ', (error as Error).message);
        }
    }
}