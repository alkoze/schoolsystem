import Axios from 'axios';
import { ILocation } from '../domain/IGpsSession';
import { ILocationDTO } from '@/types/ILocationDTO';
export abstract class GpsSessionsApi {
    private static axios = Axios.create(
        {
            baseURL: "https://localhost:5001/api/v1/Locations/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )
    static async getAll(): Promise<ILocation[]> {
        const url = "";
        try {
            const response = await this.axios.get<ILocation[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async create(locationDTO: ILocationDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<ILocationDTO>(url, locationDTO);
            console.log('location response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }
}
