import Axios from 'axios';
import { ISemesters } from '../domain/ISemesters';

export abstract class SemesterApi {
  private static axios = Axios.create(
      {
          baseURL: "https://localhost:5001/api/v1/Semesters/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<ISemesters[]> {
      const url = "";
      try {
          const response = await this.axios.get<ISemesters[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}
